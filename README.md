# ics-ans-role-spinnaker

Ansible role to install [Spinnaker SDK](https://www.flir.com/products/spinnaker-sdk/).

## Role Variables

```yaml
spinnaker_packages:
  - spinview-qt1
  - spinview-qt1-dev
  - spinupdate1
  - spinupdate1-dev
  - spinnaker
  - spinnaker-doc
spinnaker_group: flirimaging
spinnaker_users: []
spinnaker_rmem: "10485760"
spinnaker_flir_setip_git_repo: https://gitlab.esss.lu.se/icshwi/nss-instruments/flir-spinnaker-setip.git
spinnaker_flir_setip_git_version: master
```

The users from `spinnaker_users` will be added to the `spinnaker_group` to have access to USB hardware.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-spinnaker
```

## License

BSD 2-clause
