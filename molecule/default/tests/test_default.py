import os
import testinfra.utils.ansible_runner
import pytest


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize("package", ["spinview-qt1", "spinnaker"])
def test_spinnaker_installed(host, package):
    assert host.package(package).is_installed


@pytest.mark.parametrize(
    "key,value",
    [
        ("net.ipv4.conf.default.rp_filter", 0),
        ("net.ipv4.conf.all.rp_filter", 0),
        ("net.core.rmem_max", 10485760),
        ("net.core.rmem_default", 10485760),
    ],
)
def test_kernel_parameters(host, key, value):
    assert host.sysctl(key) == value


def test_setip_present(host):
    assert host.file("/usr/src/spinnaker/bin/setIP").exists
